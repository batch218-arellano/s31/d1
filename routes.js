
const http = require("http");

// Creates a variable "port" to store the port number 
// 3000, 4000, 5000, 8000 - Usually used for web development.
const port = 3000;

const server = http.createServer(function(request, response){
	// We will create two endpoint route for "/greeting" and "/homepage" and will return a response upon accessing. 
	// The "url" 

	if(request.url == "/"){
		response.writeHead(200,{"Content-Type" : "text/plain"});
		response.end("Welcome to my page");
	}

	else if(request.url == "/homepage"){
		response.writeHead(200,{"Content-Type" : "text/plain"});
		response.end("You are now in my homepage");
	}

	else{ // block of code if incase the endpoint or url does not match.
         response.writeHead(404,{"Content-Type" : "text/plain"});
         response.end("Page not available");
	}

	// Mini Activity
	// Create another 
});

server.listen(port);
console.log (`Server is now accessible at localhost: ${port}`); 